<?php declare(strict_types=1);
/*
 * Citypay payment gateway plugin for WooCommerce.
 * Copyright (C) 2019 Eugene P. <pieu@mail.ru>
 *
 * This file is part of Citypay Plugin.
 *
 * Citypay Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Citypay Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Citypay Plugin.  If not, see <https://www.gnu.org/licenses/>.
 */

defined('ABSPATH') or exit;

class Config
{
    const STATUS_OK = 0;
    const STATUS_INTERNAL_ERROR = 2;
    const STATUS_ACCOUNT_NOT_FOUND = 21;
    const STATUS_PAYMENT_FORBIDDEN = 22;
    const STATUS_ACCOUNT_INACTIVE = 24;
    const STATUS_LESS_AMOUNT = 241;
    const STATUS_MORE_AMOUNT = 242;

    const WC_GATEWAY_CITYPAY_VERSION = 1;
    const RESPONSE = 'XML'; // XML | JSON

    const REST_NAMESPACE = 'citypay/v' . self::WC_GATEWAY_CITYPAY_VERSION;

    const PAYMENT_TABLE = 'citypay_payment_transactions';
    const REVERT_TABLE = 'citypay_revert_transactions';

    const MSG_PAYMENT_LABEL = 'Укажите желаемый способ оплаты: ';
    const MSG_TERMINAL = 'Оплата через платежный терминал';
    const MSG_SITE = 'Оплата через ';
    const MSG_PAY = 'Оплатить через City-pay';
}
