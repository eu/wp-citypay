<?php declare(strict_types=1);
/*
   Plugin Name: Citypay - WooCommerce Gateway
   Description: Extends WooCommerce by Adding the City-pay Gateway.
   Version: 1.0a
   Author: Eugene P.
   Author URI: mailto:pieu@mail.ru?subject=Citypay
   License: GPLv3

 * Citypay payment gateway plugin for WooCommerce.
 * Copyright (C) 2019 Eugene P. <pieu@mail.ru>
 *
 * This file is part of Citypay Plugin.
 *
 * Citypay Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Citypay Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Citypay Plugin.  If not, see <https://www.gnu.org/licenses/>.
 */

defined('ABSPATH') or exit;

function wc_citypay_add_gateway($methods)
{
    $methods[] = 'WC_Gateway_Citypay';
    return $methods;
}

function wc_citypay_init()
{
    if (!class_exists('WC_Payment_Gateway'))
        return;

    require_once plugin_basename('config.php');
    require_once plugin_basename('inc/citypay.php');
    require_once plugin_basename('inc/routes.php');

    add_filter('woocommerce_payment_gateways', 'wc_citypay_add_gateway');
    add_action('rest_api_init', function() { citypay_register_routes(); });
}

add_action('plugins_loaded', 'wc_citypay_init');

