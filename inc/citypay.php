<?php declare(strict_types=1);
/*
 * Citypay payment gateway plugin for WooCommerce.
 * Copyright (C) 2019 Eugene P. <pieu@mail.ru>
 *
 * This file is part of Citypay Plugin.
 *
 * Citypay Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Citypay Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Citypay Plugin.  If not, see <https://www.gnu.org/licenses/>.
 */

defined('ABSPATH') or exit;

require_once 'gateway.php';
require_once 'whitelist.php';
require_once 'transaction.php';
require_once 'response.php';

class Citypay
{
    private $debug_mode;
    private $transaction;
    private $data;
    private $response;

    public function __construct(WP_REST_Request $request)
    {
	$type = 'Response' . Config::RESPONSE;
	$this->response = new $type;

        $citypay_gateway = WC()->payment_gateways->payment_gateways()['citypay'];

    	$this->debug_mode = ($citypay_gateway->get_option('debug') === 'yes') ? 1 : 0;

	if ($citypay_gateway->get_option('https') === 'yes' &&
	    (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] !== 'on'))
	    $this->error('API require HTTPS!');

        $whitelist = new WhiteList($citypay_gateway->get_option('whitelist'));
        if (!$whitelist->check_ip($_SERVER['REMOTE_ADDR']))
	    $this->error('Access for this IP is denied!');

	$this->transaction = new Transaction;
	if (!$this->transaction->init())
	    $this->error('#1 Internal Error!');

	$this->data = $request->get_params();
    }

    private function error(string $msg, int $code=Config::STATUS_INTERNAL_ERROR)
    {
	$response = [ 'ResultCode' => $code ];

	if ($this->debug_mode)
	    $response += [ 'Comment' => $msg ];

	$this->response->send($response);
    }

    public function timestamp(string $date): ?int
    {
        $d = date_create_from_format('YmdGis', $date);
	return ($d === false) ? null : date_timestamp_get($d);
    }

    /*
     * QueryType = check
     * TransactionId = 1234561
     * Account = 2128506
     *
     * <Response>
     *   <TransactionId> 1234561 </TransactionId>
     *   <ResultCode> 0 </ResultCode>
     *   <Comment> </Comment>
     * </Response>
     */
    private function check($order)
    {
	if (!$order->has_status('pending') && !$order->has_status('on-hold'))
	    $this->error('Order does not await payment!', Config::STATUS_ACCOUNT_INACTIVE);

	$response = [
	    'TransactionId' => $this->data['TransactionId'],
	    'ResultCode' => Config::STATUS_OK
	];

	if ($this->debug_mode)
	    $response += [ 'Comment' => 'Checked' ];

	$this->response->send($response);
    }

    /*
     * QueryType = pay
     * TransactionId = 1234567
     * TransactionDate = 20080625120101
     * Account = 2128506
     * Amount = 17.40
     *
     * <Response>
     *   <TransactionId> 1234567 </TransactionId>
     *   <TransactionExt> 2016 </TransactionExt>
     *   <Amount> 17.40 </Amount>
     *   <ResultCode> 0 </ResultCode>
     *   <Comment> :) </Comment>
     * </Response>
     */
    private function pay($order)
    {
	$data = $this->data;

	if (!isset($data['TransactionDate'], $data['Amount']))
	    $this->error('Invalid Request');

	if ($order->has_status('processing') || $order->has_status('completed'))
	    $this->error('Оrder has already been paid!', Config::STATUS_ACCOUNT_INACTIVE);

	if (!$order->has_status('pending') && !$order->has_status('on-hold'))
	    $this->error('Payment forbidden!', Config::STATUS_PAYMENT_FORBIDDEN);

	if(is_null($timestamp = $this->timestamp($data['TransactionDate'])))
	    $this->error('Invalid TransactionDate!');

	if ($this->transaction->pay((int) $data['TransactionId'], $data['TransactionDate'],
	    (int) $data['Account'], $data['Amount']) === false)
	    $this->error('#2 Internal Error!');

	$order->set_transaction_id($data['TransactionId']);
	$order->set_date_paid($timestamp);
	$order->payment_complete();

	$response = [
	    'TransactionId' => $data['TransactionId'],
	    'TransactionExt' => $data['TransactionId'],
	    'Amount' => number_format((float) $data['Amount'], 2, '.', ''),
	    'ResultCode' => Config::STATUS_OK
	];

	if ($this->debug_mode)
	    $response += [ 'Comment' => 'Paid' ];

	$this->response->send($response);
    }

    /*
     * QueryType = cancel
     * TransactionId = 1234567
     * RevertId = 1234579
     * RevertDate = 20080625120101
     * Account = 2128506
     * Amount = 17.40
     *
     * <Response>
     *   <TransactionId> 1234567 </TransactionId>
     *   <RevertId> 1234579 </RevertId>
     *   <TransactionExt> 2017 </TransactionExt>
     *   <Amount> 17.40 </Amount>
     *   <ResultCode> 0 </ResultCode>
     *   <Comment> :( </Comment>
     * </Response>
     */
    private function cancel($order)
    {
	$data = $this->data;

	if (!isset($data['RevertId'], $data['RevertDate'], $data['Account'], $data['Amount']))
	    $this->error('Invalid Request');

        if (!$order->has_status('processing'))
	    $this->error('Status != processing!', Config::STATUS_PAYMENT_FORBIDDEN);

	if(is_null($timestamp = $this->timestamp($data['RevertDate'])))
	    $this->error('Invalid RevertDate!');

	if ($this->transaction->revert((int) $data['TransactionId'], (int) $data['RevertId'],
		$data['RevertDate'], (int) $data['Account'], $data['Amount']) === false)
	    $this->error('#3 Internal Error!');

        $order->set_transaction_id($data['TransactionId']);
	$order->set_date_paid($timestamp);
	$order->update_status('failed');

	$response = [
	    'TransactionId' => $data['TransactionId'],
	    'RevertId' => $data['RevertId'],
	    'TransactionExt' => $data['TransactionId'],
	    'Amount' => number_format((float) $data['Amount'], 2, '.', ''),
	    'ResultCode' => Config::STATUS_OK
	];
	if ($this->debug_mode)
	    $response += [ 'Comment' => 'Canceled' ];

	$this->response->send($response);
    }

    public function check_transaction(string $type, int $id): ?array
    {
	switch ($type) {
	case 'pay':
	    return $this->transaction->payment_exists($id);
	case 'cancel':
	    return $this->transaction->revert_exists($id);
	}
	return null;
    }

    public function query()
    {
	$data = $this->data;

	if (!isset($data['TransactionId'], $data['Account']))
	    $this->error('Invalid Request');

	$response = $this->check_transaction($data['QueryType'], $data['TransactionId']);
	if (!is_null($response)) {
	    if ($this->debug_mode)
		$response += [ 'Comment' => 'TransactionId Exists' ];
	    $this->response->send($response);
	}

	$oid = (int) $data['Account'];
        $order = wc_get_order($oid);
        if (!$order instanceof WC_Order)
	    $this->error('Order not found!', Config::STATUS_ACCOUNT_NOT_FOUND);

	if ($data['QueryType'] === 'check')
	    $this->check($order);

	if (isset($data['Amount'])) {
    	    // $intval = (int) ((float) $strval * 100);
	    $amount = (int) ((float) $data['Amount'] * 100);
	    $total = (int) ((float) $order->get_total() * 100);

	    if ($amount > $total)
		$this->error('Invalid Amount!', Config::STATUS_MORE_AMOUNT);
	    if ($amount < $total)
		$this->error('Invalid Amount!', Config::STATUS_LESS_AMOUNT);
	}

	switch ($data['QueryType']) {
	case 'pay':
	    $this->pay($order);
	case 'cancel':
	    $this->cancel($order);
	}

	$this->error('Invalid Query Type!');
    }

    /*
     * CheckDateBegin = 20080625000000
     * CheckDateEnd = 20080625235959
     *
     * <Response>
     *   <Payment>
     *     <TransactionId> 1234568 </TransactionId>
     *     <Account> 2128507 </Account>
     *     <TransactionDate> 20080625120202 </TransactionDate>
     *     <Amount> 117.40 </Amount>
     *   </Payment>
     *   <Payment>
     *     <TransactionId> 1234571 </TransactionId>
     *     ...
     *   </Payment>
     * </Response>
     */
    public function reconciliation()
    {
	require_once 'auth.php';

	$citypay_gateway = WC()->payment_gateways->payment_gateways()['citypay'];

	$method = $citypay_gateway->get_option('auth_method');
	$user = $citypay_gateway->get_option('user');
	$pass = $citypay_gateway->get_option('pass');

	$auth = new Authorization($method);
	$auth = $auth->getMethod();

	if (empty($auth) || !$auth->check($user, $pass))
	    $this->error(strtoupper($method).' Auth error!');

	$data = $this->data;
	if (!isset($data['CheckDateBegin'], $data['CheckDateEnd']))
	    $this->error('Invalid Request');

	$from = $data['CheckDateBegin'];
	$to = $data['CheckDateEnd'];
	$payments = $this->transaction->successful_for_period($from, $to);

	$this->response->send($payments);
    }

    public function error_404()
    {
	$this->response->error_404();
    }
}
