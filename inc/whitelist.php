<?php declare(strict_types=1);
/*
 * Citypay payment gateway plugin for WooCommerce.
 * Copyright (C) 2019 Eugene P. <pieu@mail.ru>
 *
 * This file is part of Citypay Plugin.
 *
 * Citypay Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Citypay Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Citypay Plugin.  If not, see <https://www.gnu.org/licenses/>.
 */

defined('ABSPATH') or exit;

class WhiteList
{
    private $whitelist;

    public function __construct(string $list)
    {
        $this->whitelist = preg_split('/[\s,;]+/', $list);
    }

    public function check_ip(string $ip): bool
    {
	if (($ip = ip2long($ip)) === false)
	    return false;

	foreach ($this->whitelist as $cidr) {
	    if (!preg_match('#/#', $cidr))
		$cidr .= '/';

	    list($subnet, $mask) = explode('/', $cidr, 2);
	    $subnet = ip2long($subnet);

	    if ($subnet === false)
		continue;

	    $mask = $mask === '' ? 32 : (int) $mask;

	    if (($ip & ~((1 << (32 - $mask)) - 1)) == $subnet)
		return true;
	}
	return false;
    }
}
