<?php declare(strict_types=1);
/*
 * Citypay payment gateway plugin for WooCommerce.
 * Copyright (C) 2019 Eugene P. <pieu@mail.ru>
 *
 * This file is part of Citypay Plugin.
 *
 * Citypay Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Citypay Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Citypay Plugin.  If not, see <https://www.gnu.org/licenses/>.
 */

defined('ABSPATH') or exit;

require_once 'citypay.php';

/*
 * Citypay REST API Routes
 */
function citypay_register_routes(): void
{
    register_rest_route(Config::REST_NAMESPACE, 'query', [
	'methods' => 'GET',
	'callback' => function(WP_REST_Request $request) {
	    $cp = new Citypay($request);
	    $cp->query();
	},
	'args' => [
	    'QueryType' => ['type' => 'string', 'required' => 'true'],
	    'TransactionId' => ['type' => 'integer', 'required' => 'true'],
	    'TransactionDate' => ['type' => 'string'],
	    'Account' => ['type' => 'string'],
	    'Amount' => ['type' => 'float'],
	    'RevertId' => ['type' => 'integer'],
	    'RevertDate' => ['type' => 'string']
	]
    ]);

    register_rest_route(Config::REST_NAMESPACE, '/reconciliation', [
	'methods' => 'GET',
	'callback' => function(WP_REST_Request $request) {
	    $cp = new Citypay($request);
	    $cp->reconciliation();
	},
	'args' => [
	    'CheckDateBegin' => ['type' => 'string', 'required' => 'true'],
	    'CheckDateEnd' => ['type' => 'string', 'required' => 'true'],
	]
    ]);

    register_rest_route(Config::REST_NAMESPACE, '(.*)', [
	'methods' => 'GET',
	'callback' => function(WP_REST_Request $request) {
	    $cp = new Citypay($request);
	    $cp->error_404();
	},
	'args' => []
    ]);
}

