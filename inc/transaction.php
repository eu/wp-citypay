<?php declare(strict_types=1);
/*
 * Citypay payment gateway plugin for WooCommerce.
 * Copyright (C) 2019 Eugene P. <pieu@mail.ru>
 *
 * This file is part of Citypay Plugin.
 *
 * Citypay Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Citypay Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Citypay Plugin.  If not, see <https://www.gnu.org/licenses/>.
 */

defined('ABSPATH') or exit;

class Transaction
{
    private $payment_table;
    private $revert_table;

    public function __construct()
    {
	global $wpdb;
	$this->payment_table = $wpdb->prefix . Config::PAYMENT_TABLE;
	$this->revert_table = $wpdb->prefix . Config::REVERT_TABLE;
    }

    public function init(): bool
    {
	global $wpdb;

	if ($wpdb->get_var("SHOW TABLES LIKE '$this->payment_table'") !==
	    $this->payment_table) {
	    $q = "CREATE TABLE IF NOT EXISTS $this->payment_table (".
		 '`TransactionId` bigint(20) unsigned NOT NULL, '.
		 '`TransactionDate` datetime NOT NULL, '.
		 '`Account` bigint(20) unsigned NOT NULL, `Amount` decimal(8,2) NOT NULL, '.
		 'PRIMARY KEY (TransactionId), KEY `TransactionDate` (`TransactionDate`))';
	    if ($wpdb->query($q) === false)
		return false;
	}

	if ($wpdb->get_var("SHOW TABLES LIKE '$this->revert_table'") !==
	    $this->revert_table) {
	    $q = "CREATE TABLE IF NOT EXISTS $this->revert_table (".
		 '`TransactionId` bigint(20) unsigned NOT NULL, '.
		 '`RevertId` bigint(20) unsigned NOT NULL, `RevertDate` datetime NOT NULL, '.
		 '`Account` char(36) NOT NULL, `Amount` decimal(8,2) NOT NULL, '.
		 'PRIMARY KEY (`TransactionId`), KEY `RevertId` (`RevertId`))';
	    if ($wpdb->query($q) === false)
		return false;
	}
	return true;
    }

    public function payment_exists(int $TransactionId): ?array
    {
	global $wpdb;

	$q = "SELECT TransactionId, Amount FROM `$this->payment_table` ".
	     "WHERE TransactionId = $TransactionId";
	$res = $wpdb->get_row($q, ARRAY_A);
	return is_null($res) ? null : [
	    'TransactionId' => $res['TransactionId'],
	    'TransactionExt' => $res['TransactionId'],
	    'Amount' => $res['Amount'],
	    'ResultCode' => Config::STATUS_OK
	];
    }

    public function pay(int $TransactionId, string $TransactionDate, int $Account,
		        string $Amount): bool
    {
	global $wpdb;
	$data = [
	    'TransactionId' => $TransactionId,
	    'TransactionDate' => $TransactionDate,
	    'Account' => $Account,
	    'Amount' => $Amount
	];
	return (bool) $wpdb->insert($this->payment_table, $data);
    }

    public function revert_exists(int $TransactionId): ?array
    {
	global $wpdb;

	$q = "SELECT TransactionId, RevertId, Amount FROM `$this->revert_table` ".
	     "WHERE TransactionId = $TransactionId";
	$res = $wpdb->get_row($q, ARRAY_A);
	return is_null($res) ? null : [
	    'TransactionId' => $res['TransactionId'],
	    'RevertId' => $res['RevertId'],
	    'TransactionExt' => $res['TransactionId'],
	    'Amount' => $res['Amount'],
	    'ResultCode' => Config::STATUS_OK
	];
    }

    public function revert(int $TransactionId, int $RevertId, string $RevertDate,
			   int $Account, string $Amount): bool
    {
	global $wpdb;
	$data = [
	    'TransactionId' => $TransactionId,
	    'RevertId' => $RevertId,
	    'RevertDate' => $RevertDate,
	    'Account' => $Account,
	    'Amount' => $Amount
	];
	return (bool) $wpdb->insert($this->revert_table, $data);
    }

    public function successful_for_period(string $from, string $to)
    {
	global $wpdb;

	$q = 'SELECT TransactionId, Account, DATE_FORMAT(`TransactionDate`, "%Y%m%d%H%i%s") '.
	     "AS TransactionDate, Amount FROM `$this->payment_table` ".
	     "WHERE `TransactionDate` >= STR_TO_DATE('$from', '%Y%m%d%H%i%s') AND ".
	     "`TransactionDate` <= STR_TO_DATE('$to', '%Y%m%d%H%i%s') ".
	     "AND NOT EXISTS (SELECT null FROM `$this->revert_table` ".
	     "WHERE `RevertId` = `$this->payment_table`.`TransactionId`)";
	$res = $wpdb->get_results($q, ARRAY_A);

	return is_null($res) ? null : $res;
    }
}

