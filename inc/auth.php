<?php declare(strict_types=1);
/*
 * Citypay payment gateway plugin for WooCommerce.
 * Copyright (C) 2019 Eugene P. <pieu@mail.ru>
 *
 * This file is part of Citypay Plugin.
 *
 * Citypay Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Citypay Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Citypay Plugin.  If not, see <https://www.gnu.org/licenses/>.
 */

defined('ABSPATH') or exit;

class Authorization
{
    private $method = null;

    public function __construct(string $method)
    {
	switch ($method)
	{
	case 'basic':
	    $this->method = new BasicAuth;
	    break;
	case 'digest':
	    $this->method = new DigestAuth;
	    break;
	case 'disable':
	    $this->method = new DumbAuth;
	    break;
	}
    }

    public function getMethod(): ?Auth
    {
	return $this->method;
    }
}

abstract class Auth
{
    protected $realm = 'API';

    abstract public function check(string $user, string $pass): bool;
}

class DumbAuth extends Auth
{
    public function check(string $user, string $pass): bool
    {
	return true;
    }
}

class BasicAuth extends Auth
{
    public function check(string $user, string $pass): bool
    {
	header('WWW-Authenticate: Basic realm="' . $this->realm . '"');
	header('HTTP/1.0 401 Unauthorized');

	if (!isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']))
	    return false;

	header('HTTP/1.1 200 OK');

	return ($_SERVER['PHP_AUTH_USER'] === $user &&
		$_SERVER['PHP_AUTH_PW'] === $pass) ? true : false;
    }
}

class DigestAuth extends Auth
{
    public function check(string $user, string $pass): bool
    {
	if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
	    header('HTTP/1.1 401 Unauthorized');
	    header('WWW-Authenticate: Digest realm="' . $this->realm.
		'",qop="auth",nonce="' . uniqid() . '",opaque="' . md5($this->realm) . '"');
	    return false;
	}

	$default = [ 'nounce', 'nc', 'cnounce', 'qop', 'username', 'uri', 'response' ];
	$digest = stripcslashes($_SERVER['PHP_AUTH_DIGEST']);
	preg_match_all('~(\w+)="?([^",]+)"?~', $digest, $matches);
	$data = array_combine($matches[1] + $default, $matches[2]);

	$A1 = md5($data['username'] . ':' . $this->realm . ':' . $pass);
	$A2 = md5($_SERVER['REQUEST_METHOD'] . ':' . $data['uri']);
	$valid = md5($A1 . ':' . $data['nonce'] . ':' . $data['nc'] . ':'.
		     $data['cnonce'] . ':' . $data['qop'] . ':' . $A2);
	return ($data['username'] === $user && $data['response'] === $valid) ? true : false;
    }
}
