<?php declare(strict_types=1);
/*
 * Citypay payment gateway plugin for WooCommerce.
 * Copyright (C) 2019 Eugene P. <pieu@mail.ru>
 *
 * This file is part of Citypay Plugin.
 *
 * Citypay Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Citypay Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Citypay Plugin.  If not, see <https://www.gnu.org/licenses/>.
 */

defined('ABSPATH') or exit;

abstract class Response
{
    abstract public function send(array $data);

    public function error_404()
    {
	header("HTTP/1.0 404 Not Found");
	exit('404 Route Not Found!');
    }
}

class ResponseXML extends Response
{
    public function send(array $data)
    {
	header('Content-Type: text/xml');

        echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
	echo '<Response>' . PHP_EOL;
	foreach ($data as $key => $value) {
	    if (!is_int($key)) {
		echo "  <$key>$value</$key>\n";
		continue;
	    }
	    echo '  <Payment>' . PHP_EOL;
	    foreach ($value as $k => $v)
		echo "    <$k>$v</$k>\n";
	    echo '  </Payment>' . PHP_EOL;
	}
	echo '</Response>' . PHP_EOL;
	exit();
    }
}

class ResponseJSON extends Response
{
    public function send(array $data)
    {
	wp_send_json($data);
    }
}

