<?php declare(strict_types=1);
/*
 * Citypay payment gateway plugin for WooCommerce.
 * Copyright (C) 2019 Eugene P. <pieu@mail.ru>
 *
 * This file is part of Citypay Plugin.
 *
 * Citypay Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Citypay Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Citypay Plugin.  If not, see <https://www.gnu.org/licenses/>.
 */

defined('ABSPATH') or exit;

/**
 * Citypay Payment Gateway
 */
class WC_Gateway_Citypay extends WC_Payment_Gateway
{
    public function __construct()
    {
	$this->version = Config::WC_GATEWAY_CITYPAY_VERSION;
	$this->id = 'citypay';
	$this->icon = apply_filters('citypay_icon', plugins_url('../logo.png', __FILE__));
	$this->has_fields = true;
	$this->method_title = 'City-pay Gateway';
	$this->method_description = 'WooCommerce Citypay Gateway';
	$this->order_button_text = Config::MSG_PAY;
	$this->supports = ['products'];
	//$this->available_countries  = [ 'UA' ];

	$this->init_form_fields();
	$this->init_settings();

	foreach ($this->settings as $setting_key => $value)
	    $this->$setting_key = $value;

	if (is_admin())
	    add_action('woocommerce_update_options_payment_gateways_'.
		$this->id, [$this, 'process_admin_options']);

	add_filter('woocommerce_checkout_fields', function($fields) {
	    unset(
		$fields['billing']['billing_company'],
		$fields['billing']['billing_address_1'],
		$fields['billing']['billing_address_2'],
		$fields['billing']['billing_city'],
		$fields['billing']['billing_country'],
		$fields['billing']['billing_postcode'],
		$fields['billing']['billing_state']
	    );
	    return $fields;
	});
    }

    public function init_form_fields()
    {
	$this->form_fields = [
	    't1' => [
		'title' => 'General settings',
		'type' => 'title'
	    ],
	    'enabled' => [
		'title' => 'Enable/Disable',
		'type' => 'checkbox',
		'label' => 'Enable Citypay Payment Gateway',
		'description' => 'Enable or disable the gateway',
		'desc_tip' => true,
		'default' => 'yes'
	    ],
	    'title' => [
		'title' => 'Title',
		'type' => 'text',
		'description' => '',
		'desc_tip' => false,
		'default' => 'Citypay'
	    ],
	    'payment_site' => [
		'title' => 'Payment Site',
		'type' => 'text',
		'description' => 'Just redirect',
		'desc_tip' => true,
		'default' => 'http://localhost'
	    ],
	    'whitelist' => [
		'title' => 'Whitelist CIDR',
		'type' => 'textarea',
		'description' => 'Example 127.0.0.1 or 127.0.0.0/24',
		'default' => '127.0.0.1'
	    ],
	    'https' => [
		'title' => 'Enable/Disable',
		'type' => 'checkbox',
		'label' => 'HTTPS Only!',
		'description' => 'Enable or disable HTTPS check',
		'desc_tip' => true,
		'default' => 'yes'
	    ],
	    't2' => [
		'title' => 'Settings for reconciliation',
		'type' => 'title'
	    ],
	    'user' => [
		'title' => 'User name',
		'type' => 'text',
		'description' => '',
		'desc_tip' => false,
		'default' => 'user'
	    ],
	    'pass' => [
		'title' => 'Password',
		'type' => 'password',
		'description' => '',
		'desc_tip' => false,
		'default' => 'pass'
	    ],
	    'auth_method' => [
		'title' => 'Authentication Method',
		'description' => 'Used for reconciliation only',
		'type' => 'select',
		'options' => [
		    'disable' => 'DISABLE',
		    'basic' => 'BASIC',
		    'digest' => 'DIGEST'
		],
		'default' => 'disable'
	    ],
	    'debug' => [
		'title' => 'Debug mode',
		'type' => 'checkbox',
		'label' => 'Show comments in responses',
		'description' => 'Enable or disable debug info',
		'desc_tip' => true,
		'default' => 'yes'
	    ]
	];
    }
    public function payment_fields()
    {
	echo '  <label>' . Config::MSG_PAYMENT_LABEL . '</label>
	    <select name="citypay_payment_method">
	        <option value="terminal" selected>'. Config::MSG_TERMINAL .'</option>
	        <option value="site">' . Config::MSG_SITE . $this->get_option('payment_site').
	    '</option>
	    </select>' . PHP_EOL;
    }
    public function payment_scripts()
    {
    }

    public function validate_fields()
    {
	return true;
    }

    public function process_payment($order_id)
    {
	if (!isset($_POST['citypay_payment_method']))
	    return;
	$method = $_POST['citypay_payment_method'];

	global $woocommerce;

	$order = new WC_Order($order_id);
	$status = ($method == 'terminal') ? 'on-hold' : 'pending';
	$order->update_status($status);
	$order->reduce_order_stock();
	$woocommerce->cart->empty_cart();

	return [
	    'result' => 'success',
	    'redirect' => ($method === 'site') ? $this->get_option('payment_site') :
			  $this->get_return_url($order)
	];
    }

    public function webhook()
    {
    }
}

